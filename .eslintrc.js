module.exports = {
  'extends': 'plugin:vue/strongly-recommended',
  "parserOptions": {
    "parser": "babel-eslint",
    "ecmaVersion": 2017,
    "sourceType": "module"
  }
}