import Vue from 'vue'
import Router from 'vue-router'

const routerOptions = [
  { path: '/', component: 'Overview' },
  { path: '/wiki', component: 'Wiki' },
  { path: '/wiki/:lang', component: 'Wiki' },
  { path: '/propagation', component: 'Propagation' },
  { path: '/propagation/:to', component: 'Propagation' },
  { path: '/translator', component: 'Translator' },
  { path: '/translator/:user', component: 'Translator' },
  { path: '/translation', component: 'Translation' },
  { path: '/about', component: 'About' }
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/views/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  routes,
  base: process.env.BASE_URL,
  mode: 'history'
})
