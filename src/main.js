import Vue from 'vue'
import vuetify from './plugins/vuetify';
import App from './App'
import router from './router'
import GChart from 'vue-google-charts'

Vue.config.productionTip = false

Vue.use(GChart)

/* eslint-disable no-new */
new Vue({
  router,
  vuetify,
  render: h => h(App),
  created () {
    if (sessionStorage.redirect) {
      const redirect = sessionStorage.redirect
      delete sessionStorage.redirect
      this.$router.push(redirect)
    }
  }
}).$mount('#app')
